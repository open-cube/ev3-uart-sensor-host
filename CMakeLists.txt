cmake_minimum_required(VERSION 3.22)
project(ev3_uart_sensor_host C)

add_library(
        ev3_uart_sensor_host
        src/data_msg.c
        src/sensor.c
        src/watchdog.c
        src/messages.c
        src/sensor_info.c
        src/read_pipe.c
        src/write_pipe.c
        include/ev3/data_msg.h
        include/ev3/sensor.h
        include/ev3/watchdog.h
        include/ev3/messages.h
        include/ev3/sensor_info.h
        include/ev3/read_pipe.h
        include/ev3/write_pipe.h
        include/ev3/porting/error_codes.h
        include/ev3/porting/uart.h
        include/ev3/porting/timer.h
        include/ev3/porting/debug.h
        include/ev3/porting/compiler.h
)
target_compile_options(
        ev3_uart_sensor_host
        PRIVATE -Wall -Wextra -pedantic -ffunction-sections -fdata-sections
)
target_include_directories(
        ev3_uart_sensor_host
        PUBLIC ${CMAKE_CURRENT_SOURCE_DIR}/include
)

option(ENABLE_EV3_DEBUG "Enable debugging support")
if (ENABLE_EV3_DEBUG)
    target_compile_definitions(ev3_uart_sensor_host PUBLIC -DEV3_DEBUG)
endif ()

set_property(TARGET ev3_uart_sensor_host PROPERTY C_STANDARD 11)
set_property(TARGET ev3_uart_sensor_host PROPERTY C_EXTENSIONS OFF)


option(LINUX_DEMO "Build Linux demo program")

set(SANITIZER OFF CACHE STRING "Sanitizer type")
set_property(CACHE SANITIZER PROPERTY STRINGS ADDRESS THREAD OFF)

if (SANITIZER STREQUAL "ADDRESS")
    message(STATUS "Enabling ASan")
    target_compile_options(ev3_uart_sensor_host PUBLIC -fsanitize=address -fsanitize=undefined)
    target_link_options(ev3_uart_sensor_host PUBLIC -fsanitize=address -fsanitize=undefined)

elseif (SANITIZER STREQUAL "THREAD")
    message(STATUS "Enabling TSan")
    target_compile_options(ev3_uart_sensor_host PUBLIC -fsanitize=thread -fsanitize=undefined)
    target_link_options(ev3_uart_sensor_host PUBLIC -fsanitize=thread -fsanitize=undefined)
endif ()

if (LINUX_DEMO)
    set(THREADS_PREFER_PTHREAD_FLAG ON)
    find_package(Threads REQUIRED)

    add_executable(
            linux_demo
            demo/demo.c
            demo/linux_timer.c
            demo/linux_uart.c
            demo/linux_uart.h
            demo/linux_debug.c
    )
    target_link_libraries(linux_demo PRIVATE ev3_uart_sensor_host Threads::Threads)
    set_property(TARGET linux_demo PROPERTY C_STANDARD 11)
    set_property(TARGET linux_demo PROPERTY C_EXTENSIONS ON)
endif ()
