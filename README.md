# EV3 UART sensor host

This project contains a portable brick-side implementation of the EV3 UART sensor
protocol (see [here](https://sourceforge.net/p/lejos/wiki/UART%20Sensor%20Protocol/)).
This code is used as part of the [Open-Cube firmware](https://gitlab.fel.cvut.cz/open-cube/firmware),
but the aim of this repository is to make it portable to other chips as well.

## Porting to other platforms

TBD

## License

This code is licensed under the MIT License, see [LICENSE.md][LICENSE.md]
for details.
