#include <stdio.h>
#include <pthread.h>
#include <errno.h>
#include <stdlib.h>
#include <stdatomic.h>
#include "linux_uart.h"
#include "ev3/sensor.h"
#include <signal.h>
#include <unistd.h>
#include <inttypes.h>

typedef struct {
  ev3_sensor sensor;
  atomic_bool do_run;
  pthread_mutex_t lock;
} thread_context;

static volatile sig_atomic_t ctrl_c_pressed = false;
static void *sensor_thread(void *context);
static pthread_t start_worker_thread(thread_context *ctx);
static void register_signal_handler(void);
static void sigint_handler(int sig_no);

void print_metadata(ev3_sensor_info_basic *basic, ev3_sensor_info_ext *ext);
const char *sensor_name(int32_t id);
const char *format_name(ev3_data_format id);

int main(int argc, char **argv) {
  if (argc != 2) {
    fprintf(stderr, "one arg needed\n");
    return 1;
  }
  register_signal_handler();

  ev3_uart_handle uart = ev3_uart_open(argv[1]);
  if (uart == NULL) {
    fprintf(stderr, "failed to open uart\n");
    return 1;
  }

  thread_context ctx = {.do_run = true};
  pthread_mutex_init(&ctx.lock, NULL);
  ev3_sensor_init(uart, &ctx.sensor, true);

  pthread_t worker = start_worker_thread(&ctx);

  ev3_measurement msr;
  ev3_mode_info_basic mode;

  bool metadata_printed = false;

  while (!ctrl_c_pressed) {
    pthread_mutex_lock(&ctx.lock);
    int32_t id = ev3_sensor_identify(&ctx.sensor);
    ev3uart_error_code data_err = ev3_sensor_read(&ctx.sensor, &msr, &mode);
    pthread_mutex_unlock(&ctx.lock);

    if (id != EV3_ERROR_SENSOR_NOT_READY) {
      if (data_err == EV3_OK) {
        printf("Sensor %d, mode %d, values [", id, msr.mode);
        for (int i = 0; i < mode.values; i++) {
          int32_t value = ev3_data_get_raw_int(i, &msr, &mode);
          printf("%d%s", value, i < mode.values-1 ? ", " : "");
        }
        printf("]\n");

        if (!metadata_printed) {
          ev3_sensor_info_basic ibasic;
          ev3_sensor_info_ext iext;
          pthread_mutex_lock(&ctx.lock);
          ev3_sensor_get_metadata(&ctx.sensor, &ibasic);
          ev3_sensor_get_extended_metadata(&ctx.sensor, &iext);
          pthread_mutex_unlock(&ctx.lock);
          print_metadata(&ibasic, &iext);
          metadata_printed = true;
        }
      } else {
        printf("Initializing sensor %d\n", id);
      }
    } else {
      printf("Not connected\n");
      metadata_printed = false;
    }

    usleep(100000);
  }

  ctx.do_run = false;
  pthread_join(worker, NULL);
  ev3_sensor_deinit(&ctx.sensor, true);
  pthread_mutex_destroy(&ctx.lock);
  return 0;
}

void print_metadata(ev3_sensor_info_basic *basic, ev3_sensor_info_ext *ext) {
  printf("New sensor connected.\n");
  printf(" - Type: %d (%s)\n", basic->id, sensor_name(basic->id));
  printf(" - Speed: %"PRIu32" Bd\n", basic->baud_rate);
  printf(" - Modes: %d (visible %d)\n", basic->mode_count, ext->visible_mode_count);
  for (int i = 0; i < basic->mode_count; i++) {
    printf("   * Mode %d: %s\n", i, ext->modes[i].name);
    printf("     - Values: %dx %s\n", basic->modes[i].values, format_name(basic->modes[i].data_type));
    printf("     - SI unit: '%s'\n", ext->modes[i].unit);
    printf("     - Raw range: %.1f ~ %.1f\n", ext->modes[i].raw_range.min, ext->modes[i].raw_range.max);
    printf("     - Pct range: %.1f ~ %.1f\n", ext->modes[i].pct_range.min, ext->modes[i].pct_range.max);
    printf("     - SI  range: %.1f ~ %.1f\n", ext->modes[i].si_range.min, ext->modes[i].si_range.max);
    printf("     - Printf hint: %%%d.%df\n", ext->modes[i].printf_width, ext->modes[i].printf_decimals);
  }
}

const char *sensor_name(int32_t id) {
  switch (id) {
    case EV3_COLOR_SENSOR_ID: return "EV3 Color sensor";
    case EV3_SONIC_SENSOR_ID: return "EV3 Ultrasonic sensor";
    case EV3_GYRO_SENSOR_ID: return "EV3 Gyro sensor";
    case EV3_INFRA_SENSOR_ID: return "EV3 Infrared sensor";
    default: return "Unknown";
  }
}

const char *format_name(ev3_data_format id) {
  switch (id) {
    case EV3_FORMAT_INT_8: return "int8";
    case EV3_FORMAT_INT_16: return "int16";
    case EV3_FORMAT_INT_32: return "int32";
    case EV3_FORMAT_FLOAT: return "float";
    default: return "unknown";
  }
}

void *sensor_thread(void *context) {
  thread_context *job = context;

  while (job->do_run) {
    pthread_mutex_lock(&job->lock);
    ev3_sensor_poll(&job->sensor);
    pthread_mutex_unlock(&job->lock);
    usleep(1000);
  }

  return NULL;
}

pthread_t start_worker_thread(thread_context *ctx) {
  pthread_t tid;
  int err = pthread_create(&tid, NULL, &sensor_thread, ctx);
  if (err != 0) {
    errno = err;
    perror("failed to create worker thread");
    abort();
  }
  return tid;
}

void register_signal_handler(void) {
  struct sigaction action = {
      .sa_handler = &sigint_handler,
  };

  if (sigaction(SIGINT, &action, NULL) < 0) {
    perror("cannot set sigint handler");
    abort();
  }
}

void sigint_handler(int sig_no) {
  (void) sig_no;
  ctrl_c_pressed = true;
}
