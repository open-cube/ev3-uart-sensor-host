#include <stdio.h>
#include <stdarg.h>
#include <string.h>
#include "ev3/porting/debug.h"

void ev3_debug_printf(ev3_debug_level level, const char *file, int line, const char *fmt, ...) {
  if (level == EV3_LEVEL_DEBUG) {
    return;
  }

  const char *level_msg;
  switch (level) {
    case EV3_LEVEL_ERROR:
      level_msg = "ERROR";
      break;
    case EV3_LEVEL_WARN:
      level_msg = "WARNING";
      break;
    default:
      level_msg = "";
      break;
  }
  const char *last_file_slash = strrchr(file, '/');
  const char *filename = last_file_slash ? last_file_slash + 1 : file;
  fprintf(stderr, "EV3 %s (%s:%d): ", level_msg, filename, line);
  va_list va;
  va_start(va, fmt);
  vfprintf(stderr, fmt, va);
  va_end(va);
  fputc('\n', stderr);
}
