#include <time.h>
#include <stdio.h>
#include <stdlib.h>
#include "ev3/porting/timer.h"

uint32_t ev3_get_millis(void) {
  struct timespec ts;
  if (clock_gettime(CLOCK_MONOTONIC, &ts) < 0) {
    perror("Cannot get system time");
    abort();
  }
  return (uint32_t)(ts.tv_sec * 1000 + ts.tv_nsec / 1000000);
}
