#include <fcntl.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <errno.h>
#include <termios.h>
#include "linux_uart.h"


ev3_uart_handle ev3_uart_open(const char *tty_path) {
  int fd = EOF;
  ev3_uart *obj = NULL;

  fd = open(tty_path, O_RDWR | O_NOCTTY | O_NONBLOCK);
  if (fd < 0) {
    perror("cannot open sensor tty");
    goto error_path;
  }

  if (!isatty(fd)) {
    fprintf(stderr, "path is not a tty: %s\n", tty_path);
    goto error_path;
  }

  struct termios uart_params;
  if (tcgetattr(fd, &uart_params) < 0) {
    perror("cannot get termios params");
    goto error_path;
  }

  cfmakeraw(&uart_params);
  uart_params.c_cflag &= ~(CSIZE | PARENB | CSTOPB);
  uart_params.c_cflag |= CS8;
  uart_params.c_cc[VTIME] = 0;
  uart_params.c_cc[VMIN] = 0;
  cfsetispeed(&uart_params, B2400);
  cfsetospeed(&uart_params, B2400);

  if (tcsetattr(fd, TCSANOW, &uart_params) < 0) {
    perror("cannot set termios params");
    goto error_path;
  }

  obj = malloc(sizeof(ev3_uart));
  obj->fd = fd;
  return obj;

error_path:
  if (fd != EOF) {
    close(fd);
  }
  if (obj != NULL) {
    free(obj);
  }
  return NULL;
}

bool ev3_uart_supports_baud(ev3_uart_handle uart, uint32_t rate) {
  (void) uart;

  switch (rate) {
    case 2400:
    case 57600:
      return true;
    default:
      return false;
  }
}

ev3uart_error_code ev3_uart_set_baud(ev3_uart_handle uart, uint32_t new_rate) {
  speed_t speed;
  switch (new_rate) {
    case 2400:
      speed = B2400;
      break;
    case 57600:
      speed = B57600;
      break;
    default:
      return EV3_ERROR_INTERNAL;
  }

  tcdrain(uart->fd);

  struct termios uart_params;
  if (tcgetattr(uart->fd, &uart_params) < 0) {
    perror("cannot get termios params");
    abort();
  }

  switch (cfgetospeed(&uart_params)) {
    case B2400:
      usleep(4000);
      break;
    case B57600:
      usleep(500);
      break;
    default:
      break;
  }

  cfsetispeed(&uart_params, speed);
  cfsetospeed(&uart_params, speed);

  if (tcsetattr(uart->fd, TCSANOW, &uart_params) < 0) {
    perror("cannot set termios params");
    abort();
  }

  tcflush(uart->fd, TCIOFLUSH);

  return EV3_OK;
}

void ev3_uart_clear_fifos(ev3_uart_handle uart) {
  if (tcflush(uart->fd, TCIOFLUSH) < 0) {
    perror("cannot flush uart");
    abort();
  }
}

void ev3_uart_close(ev3_uart_handle uart) {
  if (close(uart->fd) < 0) {
    perror("cannot close sensor tty");
    abort();
  }
  uart->fd = -1;
  free(uart);
}


int32_t ev3_uart_read_byte(ev3_uart_handle uart) {
  uint8_t rd_byte = 0;
  int32_t ret;
  do {
    ret = (int32_t) read(uart->fd, &rd_byte, 1);
  } while (ret == EOF && errno == EINTR);

  if (ret != 1) {
    if (ret == 0 || errno == EAGAIN) {
      return EV3_ERROR_BUSY;
    } else {
      perror("cannot read");
      return EV3_ERROR_IO;
    }
  }

  return rd_byte;
}

int32_t ev3_uart_read_block(ev3_uart_handle uart, uint8_t *p_start, uint32_t total, uint32_t *p_done) {
  uint8_t *rd_ptr = p_start + *p_done;
  uint32_t rd_remaining = total - *p_done;
  int32_t ret;
  do {
    ret = (int32_t) read(uart->fd, rd_ptr, rd_remaining);
  } while (ret == EOF && errno == EINTR);

  if (ret < 0) {
    if (errno == EAGAIN) {
      return 0;
    } else {
      perror("cannot read");
      return EV3_ERROR_IO;
    }
  }

  *p_done += ret;
  return ret;
}

int32_t ev3_uart_write_block(ev3_uart_handle uart, const uint8_t *p_start, uint32_t total, uint32_t *p_done) {
  const uint8_t *wr_ptr = p_start + *p_done;
  uint32_t wr_remaining = total - *p_done;
  int32_t ret;

  do {
    ret = (int32_t) write(uart->fd, wr_ptr, wr_remaining);
  } while (ret == EOF && errno == EINTR);

  if (ret < 0) {
    if (errno == EAGAIN) {
      return 0;
    } else {
      perror("cannot write");
      return EV3_ERROR_IO;
    }
  }

  *p_done += ret;
  return ret;
}
