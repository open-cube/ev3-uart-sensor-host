#ifndef EV3_UART_SENSOR_HOST_LINUX_UART_H
#define EV3_UART_SENSOR_HOST_LINUX_UART_H

#include "ev3/porting/uart.h"

struct ev3_uart {
  int fd;
};

extern ev3_uart_handle ev3_uart_open(const char *tty_path);

#endif //EV3_UART_SENSOR_HOST_LINUX_UART_H
