/**
 * Helper module for storing last measurement sent over by the sensor
 */
#ifndef EV3_DATA_MSG_H
#define EV3_DATA_MSG_H

#include <stdbool.h>
#include <stdint.h>
#include "messages.h"
#include "ev3/sensor_info.h"

#ifdef __cplusplus
extern "C" {
#endif

// Structure for representing a single data packet from the sensor
typedef struct {
  // When was the packet received (might be useful for checking the age of the packet)
  uint32_t received_at_msec;
  // Raw data buffer from the sensor
  _Alignas(4) uint8_t data[EV3_MAX_DATA_BYTES];
  // Measurement mode to which the data packet corresponds
  uint8_t mode;
} ev3_measurement;

// Helper object for holding the last received data message
typedef struct {
  // Last received data message
  ev3_measurement last_msr;
  // Whether last_msr contains a valid message
  bool have_data;
} ev3_data_handler;

/**
 * Initialize the data holder object.
 * @param dh Data holder object to use.
 */
extern void ev3_data_reset(ev3_data_handler *dh);

/**
 * Parse and store the contents of a DATA message from the sensor.
 * @param dh Data holder object to use.
 * @param msg Buffer containing the received message.
 * @param len Total length of the received message.
 */
extern void ev3_data_import(ev3_data_handler *dh, const uint8_t *msg, uint32_t len);

/**
 * Query the last reading received from the sensor.
 * @param dh Data holder object to use.
 * @param out Where to put the measurement.
 * @return Whether out was updated, i.e. if the sensor has sent any data.
 */
extern bool ev3_data_read(ev3_data_handler *dh, ev3_measurement *out);

/**
 * Check whether a given sensor mode can sometimes produce valid messages
 * with an invalid checksum.
 *
 * @param sensor_id ID of the sensor
 * @param msg_header Header of the received message
 * @return True if invalid checksum is to be expected.
 */
extern bool should_ignore_data_checksum_error(uint8_t sensor_id, uint8_t msg_header);

extern int32_t ev3_data_get_raw_int(int32_t index, const ev3_measurement *in, const ev3_mode_info_basic *mode);
extern float ev3_data_get_raw_float(int32_t index, const ev3_measurement *in, const ev3_mode_info_basic *mode);
extern float ev3_data_get_pct_float(int32_t index, const ev3_measurement *in, const ev3_mode_info_basic *meta, const ev3_mode_info_ext *meta2);
extern float ev3_data_get_si_float(int32_t index, const ev3_measurement *in, const ev3_mode_info_basic *meta, const ev3_mode_info_ext *meta2);

#ifdef __cplusplus
};
#endif

#endif // EV3_DATA_MSG_H
