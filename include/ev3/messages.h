/**
 * Definitions and helper functions for decoding EV3 UART packets
 */
#ifndef EV3_MESSAGES_H
#define EV3_MESSAGES_H

#include <stdint.h>
#include "ev3/porting/compiler.h"

#ifdef __cplusplus
extern "C" {
#endif

// Known sensor IDs
typedef enum {
  EV3_COLOR_SENSOR_ID = 29,
  EV3_SONIC_SENSOR_ID = 30,
  EV3_GYRO_SENSOR_ID = 32,
  EV3_INFRA_SENSOR_ID = 33,
} ev3_sensor_id;

// Longest message that can be sent from a sensor
#define EV3_MSG_BUFFER_SIZE 35
// How often to send NACKs to sensors to keep their watchdogs in check
#define EV3_HEARTBEAT_PERIOD_MS 100
// Consider sensor dead after not sending any data after this many watchdog refreshes
#define EV3_MAX_HEARTBEATS_WITHOUT_DATA 6
// Similar to EV3_MAX_HEARTBEATS_WITHOUT_DATA, but for some quirky modes that are slow to get ready
#define EV3_MAX_HEARTBEATS_WITHOUT_DATA_LAX 100
// Maximum number of payload bytes in a DATA message
#define EV3_MAX_DATA_BYTES 32
// ID of the RGB-RAW color sensor mode (it has broken checksums)
#define EV3_COLOR_SENSOR_RGB_RAW_MODE 4
// ID of the US-SI-CM ultrasonic sensor mode (it is slow to respond to NACKs)
#define EV3_ULTRASONIC_SENSOR_SINGLE_CM_MODE 3
#define EV3_ULTRASONIC_SENSOR_SINGLE_IN_MODE 4
// Speed of the initial handshake
#define EV3_UART_HANDSHAKE_BAUD 2400
//
#define EV3_HANDSHAKE_TIMEOUT_MS 2000


#define MSG_CLASS_MASK    0xC0u
#define MSG_CLASS_OF(x) ((x) & MSG_CLASS_MASK)

// System message. Used for low-level handshaking (ACK, NACK)
#define MSG_CLASS_SYSTEM  0x00u
// Command message. Doubles as a sensor-wide typedata and a real command message.
#define MSG_CLASS_COMMAND 0x40u
// Info message. Contains a bit of information about a single sensor mode.
#define MSG_CLASS_INFO    0x80u
// Data message. Contains data in a format described by previous INFO messages.
#define MSG_CLASS_DATA    0xC0u

#define MSG_TYPE_MASK  0xC7u
#define MSG_TYPE_OF(x) ((x) & MSG_TYPE_MASK)

// Byte supposed to be sent at sensor start. Not sent.
// According to LEGO this was intended for clock synchronization.
#define MSG_TYPE_SYS_SYNC   (MSG_CLASS_SYSTEM | 0x00u)
// Retransmission request for data & simultaneous sensor watchdog refresh
#define MSG_TYPE_SYS_NACK   (MSG_CLASS_SYSTEM | 0x02u)
// Confirmation that the brick received all sensor information
#define MSG_TYPE_SYS_ACK    (MSG_CLASS_SYSTEM | 0x04u)
// Reserved?
#define MSG_TYPE_SYS_ESC    (MSG_CLASS_SYSTEM | 0x06u)

// Sensor info: type code of the sensor
#define MSG_TYPE_CMD_TYPE   (MSG_CLASS_COMMAND | 0x00u)
// Sensor info: number of supported modes
#define MSG_TYPE_CMD_MODES  (MSG_CLASS_COMMAND | 0x01u)
// Sensor info: supported DATA communication speed
#define MSG_TYPE_CMD_SPEED  (MSG_CLASS_COMMAND | 0x02u)
// Command for the sensor: switch to a different mode
#define MSG_TYPE_CMD_SELECT (MSG_CLASS_COMMAND | 0x03u)
// Raw command for the sensor. The payload has no common format,
// it is just a byte blob. It is used for e.g. firing the ultrasonic
// sensor, recalibrating the gyro or for confirming factory calibration
// on sensors.
#define MSG_TYPE_CMD_WRITE  (MSG_CLASS_COMMAND | 0x04u)

#define MSG_LEN_MASK   0x38u
#define MSG_LEN_SHIFT  3u
#define MSG_LEN_1   (0u << MSG_LEN_SHIFT)
#define MSG_LEN_2   (1u << MSG_LEN_SHIFT)
#define MSG_LEN_4   (2u << MSG_LEN_SHIFT)
#define MSG_LEN_8   (3u << MSG_LEN_SHIFT)
#define MSG_LEN_16  (4u << MSG_LEN_SHIFT)
#define MSG_LEN_32  (5u << MSG_LEN_SHIFT)
#define MSG_LEN_64  (6u << MSG_LEN_SHIFT)
#define MSG_LEN_128 (7u << MSG_LEN_SHIFT)

#define MSG_NACK     (MSG_TYPE_SYS_NACK )
#define MSG_ACK      (MSG_TYPE_SYS_ACK)
#define MSG_SYNC     (MSG_TYPE_SYS_SYNC)
#define MSG_CMD_TYPE (MSG_TYPE_CMD_TYPE | MSG_LEN_1)

#define MODE_NUMBER_MASK 0x07u
#define MODE_NUMBER_OF(x) ((x) & MODE_NUMBER_MASK)

// Sensor mode name (e.g. COL-COLOR)
#define INFO_TYPE_NAME   0x00u
// Scaling coefficients corresponding directly to how data is encoded in DATA messages
#define INFO_TYPE_RAW    0x01u
// Scaling coefficients for converting data to percent representation
#define INFO_TYPE_PCT    0x02u
// Scaling coefficients for converting data to a SI unit-based representation
#define INFO_TYPE_SI     0x03u
// SI symbol associated with this mode (e.g. "deg")
#define INFO_TYPE_SYMBOL 0x04u
// How are values encoded in the DATA message & how many of them are there
#define INFO_TYPE_FORMAT 0x80u

// how many bytes of the total is just fixed frame
#define EV3_NORMAL_MSG_FRAME_OVERHEAD 2 // initial byte + xor checksum at the end
#define EV3_INFO_MSG_FRAME_OVERHEAD 3 // initial byte + info type + xor checksum at the end

// max width of strings in INFO messages
#define EV3_MAX_SENSOR_NAME_LEN 15 // COL-REFLECT is 11
#define EV3_MAX_SENSOR_SYMBOL_LEN 7

// max mode count (EV3 has 8, SPIKE has 16)
#define EV3_MAX_SENSOR_MODE_COUNT 8

#define EV3_CMD_TYPE_BUFFER_LEN 3

extern uint32_t payload_len_in_header(uint8_t header);
extern uint8_t header_for_payload_len(uint32_t payload_size);

/**
 * Determine how many bytes have to be read/written from/to UART to transport this message.
 * @param header Header of the message.
 * @return Number of bytes in range 1-35.
 */
extern uint32_t full_length_of_message(uint8_t header);

/**
 * Calculate the XOR checksum of the EV3 packet.
 * @param buffer Data buffer over which to do the checksum.
 * @param length Length of the buffer.
 * @return XOR of all provided bytes and 0xFF.
 */
extern uint8_t calculate_message_checksum(const uint8_t *buffer, uint32_t length);

// Layout of the SPEED command message
typedef struct PACKED_STRUCT {
  uint8_t header;
  uint32_t baud_rate;
  uint8_t csum;
} ev3_cmd_speed;

// Layout of the TYPE command message
typedef struct PACKED_STRUCT {
  uint8_t header;
  uint8_t id;
  uint8_t csum;
} ev3_cmd_type;

// Layout of the MODES command message
typedef struct PACKED_STRUCT {
  uint8_t header;
  uint8_t mode_count;
  uint8_t visible_mode_count;
  uint8_t csum;
} ev3_cmd_modes;

// Layout of the INFO message containing scaling coefficients
typedef struct PACKED_STRUCT {
  uint8_t header;
  uint8_t info_type;
  float lower;
  float upper;
  uint8_t csum;
} ev3_info_limits;

// Layout of the INFO message containing data format
typedef struct PACKED_STRUCT {
  uint8_t header;
  uint8_t info_type;
  uint8_t values;
  uint8_t format;
  uint8_t figures;
  uint8_t decimals;
  uint8_t csum;
} ev3_info_format;

#ifdef __cplusplus
};
#endif

#endif // EV3_MESSAGES_H
