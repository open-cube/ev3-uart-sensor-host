#ifndef EV3_COMPILER_H
#define EV3_COMPILER_H

#ifdef __GNUC__
#define CHECK_RETURN __attribute__((warn_unused_result))
#define PACKED_STRUCT __attribute__((packed))
#define CHECK_PRINTF_FMT(fmt_arg_pos, last_arg_pos) __attribute__ ((format (printf, fmt_arg_pos, last_arg_pos)))
#else
#define CHECK_RETURN
#define PACKED
#define PRINTF_FORMAT(fmt_arg, last_arg)
#endif

#endif // EV3_COMPILER_H
