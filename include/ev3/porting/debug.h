#ifndef EV3_PORTING_DEBUG_H
#define EV3_PORTING_DEBUG_H

#include "ev3/porting/compiler.h"

typedef enum {
  EV3_LEVEL_ERROR,
  EV3_LEVEL_WARN,
  EV3_LEVEL_DEBUG
} ev3_debug_level;
extern void ev3_debug_printf(
    ev3_debug_level level,
    const char *file, int line,
    const char *fmt, ...
) CHECK_PRINTF_FMT(4, 5);

#ifdef EV3_DEBUG
#define ev3_debug(...) ev3_debug_printf(EV3_LEVEL_DEBUG, __FILE__, __LINE__, __VA_ARGS__)
#define ev3_warn(...)  ev3_debug_printf(EV3_LEVEL_WARN, __FILE__, __LINE__, __VA_ARGS__)
#define ev3_error(...) ev3_debug_printf(EV3_LEVEL_ERROR, __FILE__, __LINE__, __VA_ARGS__)
#else
#define ev3_debug(...)
#define ev3_warn(...)
#define ev3_error(...)
#endif

#endif // EV3_PORTING_DEBUG_H
