#ifndef EV3_PORTING_TIMER_H
#define EV3_PORTING_TIMER_H

#include <stdint.h>

#ifdef __cplusplus
extern "C" {
#endif

/**
 * Get a value of a system 32bit millisecond timer.
 * @return Monotonically increasing value with a wraparound at 0xFFFFFFFF.
 */
extern uint32_t ev3_get_millis(void);

#ifdef __cplusplus
};
#endif

#endif // EV3_PORTING_TIMER_H
