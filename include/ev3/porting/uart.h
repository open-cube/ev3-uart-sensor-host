#ifndef EV3_PORTING_UART_H
#define EV3_PORTING_UART_H

#include <stdint.h>
#include <stdbool.h>
#include "error_codes.h"

#ifdef __cplusplus
extern "C" {
#endif

struct ev3_uart;
typedef struct ev3_uart ev3_uart;
typedef struct ev3_uart *ev3_uart_handle;

extern bool ev3_uart_supports_baud(ev3_uart_handle uart, uint32_t rate);
extern ev3uart_error_code ev3_uart_set_baud(ev3_uart_handle uart, uint32_t rate);
extern int32_t ev3_uart_read_byte(ev3_uart_handle uart);
extern int32_t ev3_uart_read_block(ev3_uart_handle uart, uint8_t *p_start, uint32_t total, uint32_t *p_done);
extern int32_t ev3_uart_write_block(ev3_uart_handle uart, const uint8_t *p_start, uint32_t total, uint32_t *p_done);
extern void ev3_uart_clear_fifos(ev3_uart_handle uart);
extern void ev3_uart_close(ev3_uart_handle uart);

#ifdef __cplusplus
};
#endif

#endif // EV3_PORTING_UART_H
