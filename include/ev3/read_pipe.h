/**
 * Helper module for splitting a flat byte stream into individual messages.
 */
#ifndef EV3_READ_PIPE_H
#define EV3_READ_PIPE_H

#include <stdint.h>
#include "messages.h"
#include "ev3/porting/uart.h"
#include "ev3/porting/error_codes.h"

#ifdef __cplusplus
extern "C" {
#endif

// State of the stream->message FSM
typedef enum {
  // Waiting for initial synchronization
  WAITING_FOR_SYNC,
  // Synchronized; waiting for message header
  WAITING_FOR_HEADER,
  // Synchronized; receiving message body
  WAITING_FOR_BODY,
  // Synchronized; a full message is available in the RX buffer
  MESSAGE_COMPLETED,
} read_pipe_state;

// Functions for processing received messages
typedef bool (*message_callback)(void *context, const uint8_t *data, uint32_t length);

// State of the message packetizer
typedef struct {
  // Receive buffer for all messages
  uint8_t rx_buffer[EV3_MSG_BUFFER_SIZE];
  // How many bytes are valid in rx_buffer
  uint32_t rx_read_done;
  // How many valid bytes we need in rx_buffer for a full message
  uint32_t rx_read_needed;
  // State of the message reassembly FSM
  read_pipe_state state;
  // Workaround EV3 IR sensor bug
  bool ignore_twobyte_sync;
} ev3_read_pipe;

/**
 * Reset the state of the message packetizer.
 * @param st Packetizer state.
 */
extern void read_pipe_reset(ev3_read_pipe *st);

/**
 * Repeatedly read and reassemble messages from the UART port
 * until the RX FIFO is empty or an error occurs.
 *
 * This function calls st->callback for processing the
 * individual received messages.
 *
 * @param st Packetizer state.
 * @param uart UART to read from.
 */
extern ev3uart_error_code
read_pipe_poll(ev3_read_pipe *st, ev3_uart_handle uart, message_callback on_msg, void *context);

#ifdef __cplusplus
};
#endif

#endif // EV3_READ_PIPE_H
