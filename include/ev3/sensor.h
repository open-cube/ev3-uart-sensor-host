#ifndef EV3_SENSOR_H
#define EV3_SENSOR_H

#include "ev3/read_pipe.h"
#include "ev3/write_pipe.h"
#include "ev3/watchdog.h"
#include "ev3/sensor_info.h"
#include "ev3/data_msg.h"
#include "ev3/porting/uart.h"

#ifdef __cplusplus
extern "C" {
#endif

// EV3 protocol state machine
typedef enum {
  // Transitioning to 2400 Bd (initialization comm speed)
  EV3_SETTING_LOW_BAUD,
  // Receiving sensor metadata
  EV3_INIT_PHASE,
  // Sending ACK
  EV3_SENDING_ACK,
  // Transitioning to data-phase speed (typically 57600 Bd)
  EV3_SETTING_HIGH_BAUD,
  // Receiving data from the sensor
  EV3_DATA_PHASE,
  // Class has been destroyed
  EV3_DEINITIALIZED
} ev3_sensor_status;

typedef struct {
  ev3_uart_handle uart;
  ev3_read_pipe rq;
  ev3_write_pipe wq;
  ev3_watchdog wd;

  ev3_sensor_info info;
  ev3_data_handler data;

  ev3_sensor_status state;
} ev3_sensor;

extern ev3uart_error_code ev3_sensor_init(ev3_uart_handle uart, ev3_sensor *out, bool ext_metadata);
extern void ev3_sensor_reset(ev3_sensor *link);
extern void ev3_sensor_poll(ev3_sensor *link);
extern void ev3_sensor_deinit(ev3_sensor *link, bool close_uart);

extern bool ev3_sensor_is_ready(ev3_sensor *link);
extern int32_t ev3_sensor_identify(ev3_sensor *link);

extern ev3uart_error_code ev3_sensor_request_mode(ev3_sensor *link, uint8_t mode);
extern ev3uart_error_code ev3_sensor_write_command(ev3_sensor *link, const uint8_t *buffer, uint32_t size);
extern ev3uart_error_code ev3_sensor_read(ev3_sensor *link, ev3_measurement *msr, ev3_mode_info_basic *info);
extern ev3uart_error_code ev3_sensor_get_metadata(ev3_sensor *link, ev3_sensor_info_basic *info);
extern ev3uart_error_code ev3_sensor_get_extended_metadata(ev3_sensor *link, ev3_sensor_info_ext *info);

#ifdef __cplusplus
};
#endif

#endif // EV3_SENSOR_H
