/**
 * Structures for storing sensor self-description information.
 */
#ifndef EV3_SENSOR_INFO_H
#define EV3_SENSOR_INFO_H

#include <stdint.h>
#include <stdbool.h>
#include "ev3/messages.h"
#include "ev3/porting/error_codes.h"
#include "ev3/porting/uart.h"

#ifdef __cplusplus
extern "C" {
#endif

// Data type of values returned by the sensor for a given mode
typedef enum {
  EV3_FORMAT_INT_8 = 0,
  EV3_FORMAT_INT_16 = 1,
  EV3_FORMAT_INT_32 = 2,
  EV3_FORMAT_FLOAT = 3,
} ev3_data_format;

#define EV3_FORMAT_INVALID 0xFF
#define EV3_INVALID_SENSOR_ID 0

// Scaling limits
typedef struct {
  float min;
  float max;
} ev3_value_limits;

// Commonly useful information about a sensor mode
typedef struct {
  // Data type received in the data message
  ev3_data_format data_type;
  // Number of values returned
  uint8_t values;
} ev3_mode_info_basic;

// Information about a sensor
typedef struct {
  // Communication speed used during the data phase. Specified in baud.
  uint32_t baud_rate;
  // ID of the sensor (see ev3_sensor_id)
  ev3_sensor_id id;
  // Number of modes supported by the sensor
  uint8_t mode_count;
  // Information about individual modes supported by the sensor
  ev3_mode_info_basic modes[EV3_MAX_SENSOR_MODE_COUNT];
} ev3_sensor_info_basic;

// Information about a sensor mode
typedef struct {
  // Mode value range in SI units
  ev3_value_limits si_range;
  // Mode value range in percent of full scale (does not have to be 0-100!)
  ev3_value_limits pct_range;
  // Value range sent by the sensor
  ev3_value_limits raw_range;
  // Total value width (can be used for printf specification). This is likely related to the SI value.
  uint8_t printf_width;
  // How many places after a decimal point are valid (can be used for printf). This is likely related to the SI value.
  uint8_t printf_decimals;

  // Mode name (e.g. COL-REFLECT)
  char name[EV3_MAX_SENSOR_NAME_LEN+1];
  // SI unit (e.g. deg)
  char unit[EV3_MAX_SENSOR_SYMBOL_LEN+1];
} ev3_mode_info_ext;

typedef struct {
  // Number of modes that should be made visible to the user (this excludes
  // some advanced & factory-oriented modes)
  uint8_t visible_mode_count;
  // Information about individual modes supported by the sensor
  ev3_mode_info_ext modes[EV3_MAX_SENSOR_MODE_COUNT];
} ev3_sensor_info_ext;

typedef struct {
  // Small struct containing commonly used metadata
  ev3_sensor_info_basic basic;
  // Full struct for capturing all metadata
  ev3_sensor_info_ext *extended;
} ev3_sensor_info;

/**
 * Initialize an object for holding information about a sensor.
 * @param info Sensor information storage object.
 */
extern ev3uart_error_code ev3_sensor_info_init(ev3_sensor_info *info, bool collect_extended_metadata);

/**
 * Release all memory held in the sensor information object.
 * @param info Sensor information storage object.
 */
extern void ev3_sensor_info_deinit(ev3_sensor_info *info);

extern void ev3_sensor_info_reset(ev3_sensor_info *info);

/**
 * Update sensor information based on a new INFO or CMD message received from the sensor.
 * @param info Sensor information storage object.
 * @param msg Buffer containing the received message.
 * @param msg_len Total length of the received message.
 */
extern ev3uart_error_code ev3_sensor_info_add(ev3_sensor_info *info, const uint8_t *msg, uint32_t msg_len);

/**
 * Check if all required information have been sent by the sensor.
 * @param info Sensor information storage object.
 * @return True if all is OK, false otherwise.
 */
extern bool ev3_sensor_info_is_ok(ev3_sensor_info *info, ev3_uart_handle uart);

#ifdef __cplusplus
};
#endif

#endif // EV3_SENSOR_INFO_H
