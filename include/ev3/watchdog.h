/**
 * Helper module for implementing periodic sensor heartbeat.
 *
 * This allows the brick to periodically send NACK messages
 * as sensor watchdog keep-alives. Conversely, if the sensor
 * does not respond to the NACK with new data soon, it is
 * considered to be responding.
 */
#ifndef EV3_WATCHDOG_H
#define EV3_WATCHDOG_H

#include <stdbool.h>
#include <stdint.h>
#include "ev3/porting/uart.h"
#include "write_pipe.h"

#ifdef __cplusplus
extern "C" {
#endif

// State of the sensor heartbeat tracker
typedef struct {
  // timestamp at which the last NACK was sent
  uint32_t msec_of_last_nack;
  // How many data packets has the sensor sent since last NACK
  uint32_t data_packets_since_nack;
  // How many NACKs were sent without the sensor sending any DATA message back
  uint32_t nacks_without_data;
  // timestamp of CMD_TYPE
  uint32_t msec_of_cmd_type;
  // Whether the NACK timeout should be made longer
  bool ultrasonic_singleshot_workaround;
} ev3_watchdog;

typedef enum {
  SENSOR_OK,
  SENSOR_OK_SEND_NACK,
  SENSOR_GONE
} ev3_watchdog_action;

extern void ev3_watchdog_enter_data(ev3_watchdog *wd);
extern ev3_watchdog_action ev3_watchdog_update_data(ev3_watchdog *wd);
extern void ev3_watchdog_received_data(ev3_watchdog *wd);

extern void ev3_watchdog_enter_init(ev3_watchdog *wd);
extern ev3_watchdog_action ev3_watchdog_update_init(ev3_watchdog *wd);

#ifdef __cplusplus
};
#endif

#endif // EV3_WATCHDOG_H
