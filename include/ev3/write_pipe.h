/**
 * Simple queue for sensor write requests in DATA mode
 */
#ifndef EV3_WRITE_PIPE_H
#define EV3_WRITE_PIPE_H

#include <stdbool.h>
#include <stdint.h>
#include "ev3/porting/error_codes.h"
#include "ev3/porting/uart.h"
#include "ev3/messages.h"

#ifdef __cplusplus
extern "C" {
#endif

#define EV3_WQ_MODE_INVALID (-1)

// Queue for write requests
typedef struct sensor_wq {
  // Current pending message
  uint8_t tx_buffer[EV3_MSG_BUFFER_SIZE];
  uint32_t tx_done;
  uint32_t tx_needed;
  // mode switch has the third-highest priority
  int32_t mode_requested;
  // NACK has the second-highest priority
  bool nack_requested;
  // ACK has the highest priority
  bool ack_requested;
} ev3_write_pipe;

/**
 * Initialize a write queue.
 */
extern void write_pipe_reset(ev3_write_pipe *wq);

/**
 * Enqueue a new write request.
 * @param data Request data
 * @param len Length of the data
 * @return True if the request could be queued, false otherwise
 */
extern ev3uart_error_code write_pipe_push_buffer(ev3_write_pipe *wq, const uint8_t *data, uint32_t len) CHECK_RETURN;
extern void write_pipe_push_nack(ev3_write_pipe *wq);
extern void write_pipe_push_ack(ev3_write_pipe *wq);
extern void write_pipe_push_mode_switch(ev3_write_pipe *wq, uint8_t mode);
extern ev3uart_error_code write_pipe_poll(ev3_write_pipe *wq, ev3_uart_handle uart);
extern bool write_pipe_is_empty(ev3_write_pipe *wq);

#ifdef __cplusplus
};
#endif

#endif // EV3_WRITE_PIPE_H
