#include <string.h>
#include "ev3/data_msg.h"
#include "ev3/porting/timer.h"

void ev3_data_reset(ev3_data_handler *dh) {
  dh->have_data = false;
}

void ev3_data_import(ev3_data_handler *dh, const uint8_t *msg, uint32_t len) {
  unsigned int avail_data_bytes = len - EV3_NORMAL_MSG_FRAME_OVERHEAD;
  unsigned int to_copy = avail_data_bytes > EV3_MAX_DATA_BYTES ? EV3_MAX_DATA_BYTES : avail_data_bytes;

  dh->have_data = true;
  dh->last_msr.mode = MODE_NUMBER_OF(msg[0]);
  dh->last_msr.received_at_msec = ev3_get_millis();
  memcpy(&dh->last_msr.data[0], msg + 1, to_copy);
}

bool ev3_data_read(ev3_data_handler *dh, ev3_measurement *out) {
  if (dh->have_data) {
    *out = dh->last_msr;
    return true;
  } else {
    return false;
  }
}

bool should_ignore_data_checksum_error(uint8_t sensor_id, uint8_t msg_header) {
  return sensor_id == EV3_COLOR_SENSOR_ID &&
         MSG_CLASS_OF(msg_header) == MSG_CLASS_DATA &&
         MODE_NUMBER_OF(msg_header) == EV3_COLOR_SENSOR_RGB_RAW_MODE;
}

int32_t ev3_data_get_raw_int(int32_t index, const ev3_measurement *in, const ev3_mode_info_basic *mode) {
  if (index >= mode->values) {
    return 0;
  }

  switch (mode->data_type) {
    case EV3_FORMAT_INT_8: {
      int8_t value;
      memcpy(&value, in->data + 1 * index, 1);
      return value;
    }
    case EV3_FORMAT_INT_16: {
      int16_t value;
      memcpy(&value, in->data + 2 * index, 2);
      return value;
    }
    case EV3_FORMAT_INT_32: {
      int32_t value;
      memcpy(&value, in->data + 4 * index, 4);
      return value;
    }
    case EV3_FORMAT_FLOAT: {
      float value;
      memcpy(&value, in->data + 4 * index, 4);
      return (int32_t) value;
    }
    default:
      return 0;
  }
}

float ev3_data_get_raw_float(int32_t index, const ev3_measurement *in, const ev3_mode_info_basic *mode) {
  if (index >= mode->values) {
    return 0;
  }

  switch (mode->data_type) {
    case EV3_FORMAT_INT_8: {
      int8_t value;
      memcpy(&value, in->data + 1 * index, 1);
      return value;
    }
    case EV3_FORMAT_INT_16: {
      int16_t value;
      memcpy(&value, in->data + 2 * index, 2);
      return value;
    }
    case EV3_FORMAT_INT_32: {
      int32_t value;
      memcpy(&value, in->data + 4 * index, 4);
      return (float) value;
    }
    case EV3_FORMAT_FLOAT: {
      float value;
      memcpy(&value, in->data + 4 * index, 4);
      return value;
    }
    default:
      return 0;
  }
}

float ev3_data_get_pct_float(int32_t index,
    const ev3_measurement *in,
    const ev3_mode_info_basic *meta,
    const ev3_mode_info_ext *meta2) {
  float raw = ev3_data_get_raw_float(index, in, meta);

  float raw_delta = raw - meta2->raw_range.min;
  float raw_span = meta2->raw_range.max - meta2->raw_range.min;
  float pct_span = meta2->pct_range.max - meta2->pct_range.min;

  return raw_delta / raw_span * pct_span + meta2->pct_range.min;
}

float ev3_data_get_si_float(int32_t index,
    const ev3_measurement *in,
    const ev3_mode_info_basic *meta,
    const ev3_mode_info_ext *meta2) {
  float raw = ev3_data_get_raw_float(index, in, meta);

  float raw_delta = raw - meta2->raw_range.min;
  float raw_span = meta2->raw_range.max - meta2->raw_range.min;
  float si_span = meta2->si_range.max - meta2->si_range.min;

  return raw_delta / raw_span * si_span + meta2->si_range.min;
}
