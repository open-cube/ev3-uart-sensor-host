/**
 * Definitions and helper functions for decoding EV3 UART packets
 */
#include "ev3/messages.h"

uint8_t calculate_message_checksum(const uint8_t *buffer, uint32_t length) {
  uint8_t csum = 0xFF;
  for (uint32_t i = 0; i < length; i++) {
    csum ^= buffer[i];
  }
  return csum;
}

uint32_t full_length_of_message(uint8_t header) {
  if ((header & MSG_CLASS_MASK) == MSG_CLASS_SYSTEM) {
    return 1;
  }

  unsigned int payload_len = payload_len_in_header(header);

  if ((header & MSG_CLASS_MASK) == MSG_CLASS_INFO) {
    return payload_len + EV3_INFO_MSG_FRAME_OVERHEAD;
  } else {
    return payload_len + EV3_NORMAL_MSG_FRAME_OVERHEAD;
  }
}

uint32_t payload_len_in_header(uint8_t header) {
  uint8_t exponent = (header & MSG_LEN_MASK) >> MSG_LEN_SHIFT;
  uint32_t length = 1u << exponent;
  return length;
}

uint8_t header_for_payload_len(uint32_t payload_size) {
  uint8_t header;
  if (payload_size <= 1) {
    header = MSG_LEN_1;
  } else if (payload_size <= 2) {
    header = MSG_LEN_2;
  } else if (payload_size <= 4) {
    header = MSG_LEN_4;
  } else if (payload_size <= 8) {
    header = MSG_LEN_8;
  } else if (payload_size <= 16) {
    header = MSG_LEN_16;
  } else if (payload_size <= 32) {
    header = MSG_LEN_32;
  } else if (payload_size <= 64) {
    header = MSG_LEN_64;
  } else {
    header = MSG_LEN_128;
  }
  return header;
}
