/**
 * Helper module for splitting a flat byte stream into individual messages.
 */
#include <string.h>
#include "ev3/read_pipe.h"
#include "ev3/porting/debug.h"


static int32_t try_sync(ev3_read_pipe *st, ev3_uart_handle uart);
static int32_t try_read_header(ev3_read_pipe *st, ev3_uart_handle uart);
static int32_t try_read_body(ev3_read_pipe *st, ev3_uart_handle uart);

void read_pipe_reset(ev3_read_pipe *st) {
  memset(st->rx_buffer, 0, EV3_MSG_BUFFER_SIZE);
  st->state = WAITING_FOR_SYNC;
  st->rx_read_done = 0;
  st->rx_read_needed = EV3_CMD_TYPE_BUFFER_LEN;
  st->ignore_twobyte_sync = false;
}

ev3uart_error_code read_pipe_poll(ev3_read_pipe *st, ev3_uart_handle uart, message_callback on_msg, void *context) {
  int32_t read = 1;
  while (read > 0) {
    if (st->state == WAITING_FOR_SYNC) {
      read = try_sync(st, uart);
    }
    if (st->state == WAITING_FOR_HEADER && read > 0) {
      read = try_read_header(st, uart);
    }
    if (st->state == WAITING_FOR_BODY && read > 0) {
      read = try_read_body(st, uart);
    }
    if (st->state == MESSAGE_COMPLETED && read > 0) {
      bool do_continue;
      if (st->ignore_twobyte_sync && st->rx_buffer[0] == MSG_SYNC) {
        // skip broken SYNC packets from EV3 IR sensor
        do_continue = true;
      } else {
        do_continue = on_msg(context, st->rx_buffer, st->rx_read_needed);
      }
      if (do_continue) {
        st->state = WAITING_FOR_HEADER;
      } else {
        read = EV3_OK;
        break;
      }
    }
  }
  return read;
}

int32_t try_sync(ev3_read_pipe *st, ev3_uart_handle uart) {
  int32_t rx_byte = ev3_uart_read_byte(uart);
  if (rx_byte >= 0) {
    uint8_t *msg = st->rx_buffer;

    msg[0] = msg[1];
    msg[1] = msg[2];
    msg[2] = rx_byte;

    if (msg[0] == MSG_CMD_TYPE && (msg[0] ^ msg[1] ^ msg[2]) == 0xFF) {
      // synced!
      st->rx_read_needed = EV3_CMD_TYPE_BUFFER_LEN;
      st->rx_read_done = EV3_CMD_TYPE_BUFFER_LEN;
      st->state = MESSAGE_COMPLETED;
      ev3_debug("message stream synchronized");
    }

    return 1;
  } else {
    return rx_byte == EV3_ERROR_BUSY ? 0 : rx_byte;
  }
}

int32_t try_read_header(ev3_read_pipe *st, ev3_uart_handle uart) {
  int32_t rx_byte = ev3_uart_read_byte(uart);
  if (rx_byte >= 0) {
    st->rx_buffer[0] = rx_byte;
    st->rx_read_done = 1;
    if (st->ignore_twobyte_sync && rx_byte == MSG_SYNC) {
      // The IR sensor sometimes sends 0x00 0xFF packets during the
      // handshake - they look like broken SYNC packages. They are useless,
      // so just detect them here and later skip them
      st->rx_read_needed = 2;
    } else {
      st->rx_read_needed = full_length_of_message(rx_byte);
    }

    if (st->rx_read_needed > EV3_MSG_BUFFER_SIZE) {
      // message too long, fail
      ev3_error("received too long message: %d B. If this is not an error, increase EV3_MSG_BUFFER_SIZE (now %d B)",
          st->rx_read_needed, EV3_MSG_BUFFER_SIZE);
      return EV3_ERROR_INVALID_MESSAGE;
    }

    if (st->rx_read_needed == st->rx_read_done) {
      // one-byte message
      st->state = MESSAGE_COMPLETED;

    } else {
      // multi-byte message
      st->state = WAITING_FOR_BODY;
    }
    return 1;
  } else {
    return rx_byte == EV3_ERROR_BUSY ? 0 : rx_byte;
  }
}

int32_t try_read_body(ev3_read_pipe *st, ev3_uart_handle uart) {
  int32_t rx_count = ev3_uart_read_block(
      uart,
      st->rx_buffer,
      st->rx_read_needed,
      &st->rx_read_done
  );
  if (rx_count > 0 && st->rx_read_needed == st->rx_read_done) {
    // multi-byte read is complete
    st->state = MESSAGE_COMPLETED;
  }
  return rx_count;
}
