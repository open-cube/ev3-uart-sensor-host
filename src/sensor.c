#include <string.h>
#include <assert.h>
#include <inttypes.h>
#include "ev3/sensor.h"
#include "ev3/porting/debug.h"

static bool on_message(void *link_obj, const uint8_t *msg, uint32_t len);
static void on_data_message(ev3_sensor *link, const uint8_t *msg, size_t len);
static void on_init_message(ev3_sensor *link, const uint8_t *msg, size_t len);
static bool is_ultrasonic_single_mode(ev3_sensor *link, unsigned mode);

ev3uart_error_code ev3_sensor_init(ev3_uart_handle uart, ev3_sensor *out, bool ext_metadata) {
  ev3uart_error_code ret;
  out->uart = uart;
  ret = ev3_sensor_info_init(&out->info, ext_metadata);
  ev3_debug("initialized a sensor");
  if (ret == EV3_OK) {
    ev3_sensor_reset(out);
  }
  return ret;
}

void ev3_sensor_deinit(ev3_sensor *link, bool close_uart) {
  // free allocated resources
  ev3_sensor_info_deinit(&link->info);
  // drop uart
  if (close_uart) {
    ev3_uart_close(link->uart);
  }
  // drop self
  link->state = EV3_DEINITIALIZED;
  ev3_debug("deinitialized a sensor");
}


void ev3_sensor_reset(ev3_sensor *link) {
  ev3_debug("resetting all state");
  ev3_sensor_info_reset(&link->info);
  read_pipe_reset(&link->rq);
  write_pipe_reset(&link->wq);
  ev3_data_reset(&link->data);
  ev3_uart_clear_fifos(link->uart);
  link->state = EV3_SETTING_LOW_BAUD;
}

static void handle_setting_low_baud(ev3_sensor *link);
static void handle_init_phase(ev3_sensor *link);
static void handle_acking(ev3_sensor *link);
static void handle_setting_high_baud(ev3_sensor *link);
static void handle_data_phase(ev3_sensor *link);

void ev3_sensor_poll(ev3_sensor *link) {
  if (link->state == EV3_DEINITIALIZED) {
    // user code is buggy and still calls poll() even after deinit()
    ev3_error("called poll() after deinit()");
    return;
  }

  switch (link->state) {
    case EV3_SETTING_LOW_BAUD:
      handle_setting_low_baud(link);
      break;
    case EV3_INIT_PHASE:
      handle_init_phase(link);
      break;
    case EV3_SENDING_ACK:
      handle_acking(link);
      break;
    case EV3_SETTING_HIGH_BAUD:
      handle_setting_high_baud(link);
      break;
    case EV3_DATA_PHASE:
      handle_data_phase(link);
      break;
    default:
      break;
  }
}


void handle_setting_low_baud(ev3_sensor *link) {
  // ignore non-EV3_ERROR_BUSY errors here - the struct is basically already in reset state
  if (ev3_uart_set_baud(link->uart, EV3_UART_HANDSHAKE_BAUD) == EV3_OK) {
    ev3_debug("done switching to init phase baudrate");
    link->state = EV3_INIT_PHASE;
  }
}

void handle_init_phase(ev3_sensor *link) {
  if (read_pipe_poll(&link->rq, link->uart, &on_message, link) != EV3_OK) {
    ev3_error("read pipe polling returned an error during init phase");
    ev3_sensor_reset(link);
  }
  if (link->info.basic.id != EV3_INVALID_SENSOR_ID &&
      ev3_watchdog_update_init(&link->wd) == SENSOR_GONE) {
    ev3_error("sensor timed out during handshake");
    ev3_sensor_reset(link);
  }
}

void handle_acking(ev3_sensor *link) {
  if (write_pipe_poll(&link->wq, link->uart) != EV3_OK) {
    ev3_error("write pipe polling returned an error during ACK wait");
    ev3_sensor_reset(link);
    return;
  }
  if (write_pipe_is_empty(&link->wq)) {
    ev3_debug("ACK sent");
    link->state = EV3_SETTING_HIGH_BAUD;
  }
}

void handle_setting_high_baud(ev3_sensor *link) {
  ev3uart_error_code err = ev3_uart_set_baud(link->uart, link->info.basic.baud_rate);

  if (err == EV3_OK) {
    // done
    ev3_watchdog_enter_data(&link->wd);
    link->state = EV3_DATA_PHASE;
    ev3_debug("done switching to data phase baudrate");

  } else if (err != EV3_ERROR_BUSY) {
    // something bad happened
    ev3_error("switching to data phase baudrate failed");
    ev3_sensor_reset(link);
  }
}

void handle_data_phase(ev3_sensor *link) {
  if (read_pipe_poll(&link->rq, link->uart, &on_message, link) != EV3_OK) {
    ev3_error("read pipe poll returned an error during data phase");
    ev3_sensor_reset(link);
    return;
  }

  switch (ev3_watchdog_update_data(&link->wd)) {
    case SENSOR_OK:
      break;
    case SENSOR_OK_SEND_NACK:
      ev3_debug("sending watchdog NACK");
      write_pipe_push_nack(&link->wq);
      break;
    case SENSOR_GONE:
      ev3_warn("sensor watchdog timed out");
      ev3_sensor_reset(link);
      return; // !!!
  }
  if (write_pipe_poll(&link->wq, link->uart) != EV3_OK) {
    ev3_error("write pipe poll returned an error during data phase");
    ev3_sensor_reset(link);
  }
}


bool on_message(void *link_obj, const uint8_t *msg, uint32_t len) {
  ev3_sensor *link = link_obj;

  if (link->state == EV3_INIT_PHASE) {
    if (len >= EV3_CMD_TYPE_BUFFER_LEN && msg[0] == MSG_CMD_TYPE) {
      link->rq.ignore_twobyte_sync = link->info.basic.id == EV3_INFRA_SENSOR_ID;
      ev3_watchdog_enter_init(&link->wd);
    }
    on_init_message(link, msg, len);
  } else if (link->state == EV3_DATA_PHASE) {
    on_data_message(link, msg, len);
  }

  return link->state == EV3_INIT_PHASE ||
         link->state == EV3_DATA_PHASE;
}

void on_init_message(ev3_sensor *link, const uint8_t *msg, size_t len) {
  if (len == 0) {
    ev3_error("bug: received init phase message with len==0");
    return;
  }

  if (len > 1) {
    // checksum error during handshake causes handshake restart
    uint8_t calculated_csum = calculate_message_checksum(msg, len - 1);
    uint8_t msg_csum = msg[len - 1];
    if (calculated_csum != msg_csum) {
      ev3_error("checksum mismatch during init phase, restarting");
      ev3_sensor_reset(link);
      return;
    }
  }

  if (msg[0] == MSG_ACK) {
    if (!ev3_sensor_info_is_ok(&link->info, link->uart)) {
      ev3_error("sensor metadata failed some checks");
      ev3_sensor_reset(link);
      return;
    }
    write_pipe_push_ack(&link->wq);
    link->state = EV3_SENDING_ACK;
  } else {
    if (ev3_sensor_info_add(&link->info, msg, len) != EV3_OK) {
      ev3_error("received an invalid metadata message");
      ev3_sensor_reset(link);
      return;
    }
  }
}

void on_data_message(ev3_sensor *link, const uint8_t *msg, size_t len) {
  if (MSG_CLASS_OF(msg[0]) != MSG_CLASS_DATA) {
    // this parser only understands DATA messages after the handshake ends
    // other classes might not be an error: sensor can reply with NACK
    return;
  }
  if (len < EV3_NORMAL_MSG_FRAME_OVERHEAD) {
    // we require at least header + checksum
    ev3_error("bug: received data message with len<2");
    return;
  }

  bool csum_ok = calculate_message_checksum(msg, len) == 0;
  bool ignore = should_ignore_data_checksum_error(link->info.basic.id, msg[0]);
  if (!csum_ok && !ignore) {
    // checksum error; NACK makes the sensor send data again
    ev3_warn("checksum mismatch during data phase, sending NACK");
    write_pipe_push_nack(&link->wq);
    return;
  }

  if (link->wd.ultrasonic_singleshot_workaround) {
    if (is_ultrasonic_single_mode(link, MODE_NUMBER_OF(msg[0]))) {
      // We received the first DATA message for the given SELECT.
      // Subsequent responses to NACK should be quick, so disable the
      // workaround now.
      // Note: this likely does not handle mixing of US-SI-CM/US-SI-IN;
      // this is ignored for keeping things simple.
      link->wd.ultrasonic_singleshot_workaround = false;
    }
  }

  // receive the data
  ev3_watchdog_received_data(&link->wd);
  ev3_data_import(&link->data, msg, len);
}

ev3uart_error_code ev3_sensor_request_mode(ev3_sensor *link, uint8_t mode) {
  ev3uart_error_code ret;
  ev3_debug("user is requesting mode %d", mode);

  if (link->state == EV3_DATA_PHASE) {
    if (mode < link->info.basic.mode_count) {
      // If we're SELECTing US-SI-CM/US-SI-IN, we may need to wait
      // quite a long time for the first DATA message. This is because
      // the sensor will hold off sending that DATA message until it can get
      // a valid measurement (which may take on the order of seconds, if
      // you put something non-sound-reflective in front of it).
      link->wd.ultrasonic_singleshot_workaround = is_ultrasonic_single_mode(link, mode);

      write_pipe_push_mode_switch(&link->wq, mode);
      ret = EV3_OK;
    } else {
      ev3_warn("attempted a mode switch to invalid mode");
      ret = EV3_ERROR_BOGUS_MODE;
    }
  } else {
    ret = EV3_ERROR_SENSOR_NOT_READY;
  }
  return ret;
}

#define MAX_DIRECT_WRITE_BYTES (16)
#define DIRECT_MSG_BUFFER (MAX_DIRECT_WRITE_BYTES + EV3_NORMAL_MSG_FRAME_OVERHEAD)
static_assert(
    EV3_MSG_BUFFER_SIZE >= DIRECT_MSG_BUFFER,
    "Write queue buffers are too small for direct writes"
);

ev3uart_error_code ev3_sensor_write_command(ev3_sensor *link, const uint8_t *buffer, uint32_t size) {
  if (size > MAX_DIRECT_WRITE_BYTES) {
    ev3_error("attempted to do a too large CMD_WRITE (got %" PRIu32
        ", max %d). If this is not an error, increase MAX_DIRECT_WRITE_BYTES",
        size, MAX_DIRECT_WRITE_BYTES);
    return EV3_ERROR_INVALID_MESSAGE;
  }
  if (link->state != EV3_DATA_PHASE) {
    return EV3_ERROR_SENSOR_NOT_READY;
  }

  ev3_debug("user is requesting a CMD_WRITE of %d bytes", size);
  uint8_t header = MSG_TYPE_CMD_WRITE | header_for_payload_len(size);
  uint32_t total_length = full_length_of_message(header);
  assert(total_length <= DIRECT_MSG_BUFFER);

  uint8_t msg[DIRECT_MSG_BUFFER] = {0}; // zero-initialize
  msg[0] = header;
  memcpy(&msg[1], buffer, size);
  msg[total_length - 1] = calculate_message_checksum(msg, total_length - 1);

  return write_pipe_push_buffer(&link->wq, msg, total_length);
}

bool ev3_sensor_is_ready(ev3_sensor *link) {
  return link->data.have_data;
}

ev3uart_error_code ev3_sensor_read(ev3_sensor *link, ev3_measurement *msr, ev3_mode_info_basic *info) {
  if (ev3_data_read(&link->data, msr)) {
    if (msr != NULL && info != NULL) {
      *info = link->info.basic.modes[msr->mode];
    }
    return EV3_OK;
  } else {
    return EV3_ERROR_SENSOR_NOT_READY;
  }
}

int32_t ev3_sensor_identify(ev3_sensor *link) {
  if (link->info.basic.id == EV3_INVALID_SENSOR_ID) {
    return EV3_ERROR_SENSOR_NOT_READY;
  }

  return link->info.basic.id;
}

ev3uart_error_code ev3_sensor_get_metadata(ev3_sensor *link, ev3_sensor_info_basic *info) {
  if (link->state != EV3_DATA_PHASE) {
    return EV3_ERROR_SENSOR_NOT_READY;
  }

  if (info) {
    *info = link->info.basic;
  }
  return EV3_OK;
}

ev3uart_error_code ev3_sensor_get_extended_metadata(ev3_sensor *link, ev3_sensor_info_ext *info) {
  if (link->state != EV3_DATA_PHASE) {
    return EV3_ERROR_SENSOR_NOT_READY;
  }
  if (link->info.extended == NULL) {
    return EV3_ERROR_EXT_METADATA_DISABLED;
  }

  if (info) {
    *info = *link->info.extended;
  }
  return EV3_OK;
}

bool is_ultrasonic_single_mode(ev3_sensor *link, unsigned mode) {
  return link->info.basic.id == EV3_SONIC_SENSOR_ID &&
    (mode == EV3_ULTRASONIC_SENSOR_SINGLE_CM_MODE ||
     mode == EV3_ULTRASONIC_SENSOR_SINGLE_IN_MODE);
 }
