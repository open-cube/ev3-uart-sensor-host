/**
 * Decoder for sensor self-description information.
 */
#include <string.h>
#include <malloc.h>
#include "ev3/sensor_info.h"
#include "ev3/porting/debug.h"

typedef enum {
  RANGE_SI, RANGE_PCT, RANGE_RAW
} ev3_range_type;

static ev3uart_error_code ev3_sensor_handle_cmd(ev3_sensor_info *info, const uint8_t *msg, uint32_t len);
static ev3uart_error_code ev3_sensor_update_type(ev3_sensor_info *info, const uint8_t *msg, uint32_t len);
static ev3uart_error_code ev3_sensor_update_modes(ev3_sensor_info *info, const uint8_t *msg, uint32_t len);
static ev3uart_error_code ev3_sensor_update_speed(ev3_sensor_info *info, const uint8_t *msg, uint32_t len);
static ev3uart_error_code ev3_sensor_handle_info(ev3_sensor_info *info, const uint8_t *msg, uint32_t len);
static ev3uart_error_code ev3_mode_update_name(ev3_sensor_info *info, const uint8_t *msg, uint32_t len);
static ev3uart_error_code ev3_mode_update_symbol(ev3_sensor_info *info, const uint8_t *msg, uint32_t len);
static ev3uart_error_code
ev3_mode_update_range(ev3_sensor_info *info, ev3_range_type which, const uint8_t *msg, uint32_t len);
static ev3uart_error_code ev3_mode_update_format(ev3_sensor_info *info, const uint8_t *msg, uint32_t len);

static inline uint32_t min_u32(uint32_t a, uint32_t b) {
  return a < b ? a : b;
}


ev3uart_error_code ev3_sensor_info_init(ev3_sensor_info *info, bool collect_extended_metadata) {
  if (collect_extended_metadata) {
    // allocate room for extended metadata
    info->extended = malloc(sizeof(ev3_sensor_info_ext));

    if (info->extended == NULL) {
      ev3_error("failed to allocate memory for extended metadata");
      return EV3_ERROR_OUT_OF_MEMORY;
    }
  }
  ev3_sensor_info_reset(info);
  return EV3_OK;
}

void ev3_sensor_info_reset(ev3_sensor_info *info) {
  // initialize basic metadata defaults
  info->basic.mode_count = 0;
  info->basic.baud_rate = 0;
  info->basic.id = EV3_INVALID_SENSOR_ID;
  for (int mode = 0; mode < EV3_MAX_SENSOR_MODE_COUNT; mode++) {
    info->basic.modes[mode].values = 0;
    info->basic.modes[mode].data_type = EV3_FORMAT_INVALID;
  }

  if (info->extended != NULL) {
    // initialize extended metadata defaults
    info->extended->visible_mode_count = 0;
    for (int mode = 0; mode < EV3_MAX_SENSOR_MODE_COUNT; mode++) {
      memset(info->extended->modes[mode].unit, 0, EV3_MAX_SENSOR_SYMBOL_LEN + 1);
      memset(info->extended->modes[mode].name, 0, EV3_MAX_SENSOR_NAME_LEN + 1);
      info->extended->modes[mode].si_range.min = 0.0f;
      info->extended->modes[mode].si_range.max = 1023.0f;
      info->extended->modes[mode].pct_range.min = 0.0f;
      info->extended->modes[mode].pct_range.max = 100.0f;
      info->extended->modes[mode].raw_range.min = 0.0f;
      info->extended->modes[mode].raw_range.max = 1023.0f;
      info->extended->modes[mode].printf_width = 4;
      info->extended->modes[mode].printf_decimals = 0;
    }
  }
}

void ev3_sensor_info_deinit(ev3_sensor_info *info) {
  if (info->extended != NULL) {
    free(info->extended);
    info->extended = NULL;
  }
}

#define REQUIRE(x, ...) do { \
  if (!(x)) {                \
    return false;            \
    ev3_error(__VA_ARGS__);  \
  }                          \
} while(0)

bool ev3_sensor_info_is_ok(ev3_sensor_info *info, ev3_uart_handle uart) {
  REQUIRE(info->basic.id != EV3_INVALID_SENSOR_ID,
      "sensor did not send CMD_TYPE or sent an ID of zero");
  REQUIRE(info->basic.mode_count != 0,
      "sensor did not send CMD_FORMAT or has zero modes");
  REQUIRE(info->basic.baud_rate >= EV3_UART_HANDSHAKE_BAUD,
      "sensor sent a weird baud rate %d", info->basic.baud_rate);
  REQUIRE(ev3_uart_supports_baud(uart, info->basic.baud_rate),
      "sensor requested an unsupported baud rate %d", info->basic.baud_rate);

  for (int i = 0; i < info->basic.mode_count; i++) {
    REQUIRE(info->basic.modes[i].values > 0,
        "sensor has 0 values for mode %d", i);
    REQUIRE(info->basic.modes[i].data_type != EV3_FORMAT_INVALID,
        "sensor did not send INFO_FORMAT for mode %d", i);

    if (info->extended != NULL) {
      REQUIRE(info->extended->modes[i].name[0] != '\0',
          "sensor did not send INFO_NAME for mode %d", i);
    }
  }

  return true;
}
#undef REQUIRE

extern ev3uart_error_code ev3_sensor_info_add(ev3_sensor_info *info, const uint8_t *msg, uint32_t msg_len) {
  // ensure that at least header and checksum are there
  if (msg_len < EV3_NORMAL_MSG_FRAME_OVERHEAD) {
    ev3_error("received an INFO/CMD message smaller than 2 bytes (actual len %u)", msg_len);
    return EV3_ERROR_INVALID_MESSAGE;
  }

  switch (msg[0] & MSG_CLASS_MASK) {
    case MSG_CLASS_COMMAND:
      return ev3_sensor_handle_cmd(info, msg, msg_len);
    case MSG_CLASS_INFO:
      return ev3_sensor_handle_info(info, msg, msg_len);
    default:
      ev3_error("bug: tried to decode non-CMD/INFO message as sensor metadata, header 0x%02hhX", msg[0]);
      // only CMD and INFO messages are expected here
      return EV3_ERROR_INVALID_MESSAGE;
  }
}

ev3uart_error_code ev3_sensor_handle_cmd(ev3_sensor_info *info, const uint8_t *msg, uint32_t len) {
  switch (msg[0] & MSG_TYPE_MASK) {
    case MSG_TYPE_CMD_TYPE:
      return ev3_sensor_update_type(info, msg, len);
    case MSG_TYPE_CMD_MODES:
      return ev3_sensor_update_modes(info, msg, len);
    case MSG_TYPE_CMD_SPEED:
      return ev3_sensor_update_speed(info, msg, len);
    case MSG_TYPE_CMD_SELECT:
      ev3_error("received a CMD_SELECT from a sensor  during initialization");
      return EV3_ERROR_INVALID_MESSAGE;
    case MSG_TYPE_CMD_WRITE:
      ev3_error("received a CMD_WRITE from a sensor during initialization");
      return EV3_ERROR_INVALID_MESSAGE;
    default:
      ev3_warn("received an unknown CMD: header 0x%02hhX, please teach me about it", msg[0]);
      // ignore unknown extensions
      return EV3_OK;
  }
}

ev3uart_error_code ev3_sensor_update_type(ev3_sensor_info *info, const uint8_t *msg, uint32_t len) {
  if (len < sizeof(ev3_cmd_type)) {
    ev3_error("received too small CMD_TYPE (got %d B, needed %ld B)", len, sizeof(ev3_cmd_type));
    return EV3_ERROR_INVALID_MESSAGE;
  }

  ev3_cmd_type parsed;
  memcpy(&parsed, msg, sizeof(parsed));

  info->basic.id = parsed.id;
  return EV3_OK;
}

ev3uart_error_code ev3_sensor_update_modes(ev3_sensor_info *info, const uint8_t *msg, uint32_t len) {
  if (len < sizeof(ev3_cmd_modes)) {
    ev3_error("received too small CMD_MODES (got %d B, needed %ld B)", len, sizeof(ev3_cmd_modes));
    return EV3_ERROR_INVALID_MESSAGE;
  }

  ev3_cmd_modes parsed;
  memcpy(&parsed, msg, sizeof(parsed));

  info->basic.mode_count = min_u32(parsed.mode_count + 1, EV3_MAX_SENSOR_MODE_COUNT);
  if (info->extended != NULL) {
    info->extended->visible_mode_count =
        min_u32(parsed.visible_mode_count + 1, info->basic.mode_count);
  }

  return EV3_OK;
}

ev3uart_error_code ev3_sensor_update_speed(ev3_sensor_info *info, const uint8_t *msg, uint32_t len) {
  if (len < sizeof(ev3_cmd_speed)) {
    ev3_error("received too small CMD_SPEED (got %d B, needed %ld B)", len, sizeof(ev3_cmd_speed));
    return EV3_ERROR_INVALID_MESSAGE;
  }

  ev3_cmd_speed parsed;
  memcpy(&parsed, msg, sizeof(parsed));

  info->basic.baud_rate = parsed.baud_rate;
  return EV3_OK;
}

ev3uart_error_code ev3_sensor_handle_info(ev3_sensor_info *info, const uint8_t *msg, uint32_t len) {
  // check if info-byte is present
  if (len < EV3_INFO_MSG_FRAME_OVERHEAD) {
    ev3_error("received an INFO message smaller than 3 bytes (actual len %u)", len);
    return EV3_ERROR_INVALID_MESSAGE;
  }

  switch (msg[1]) {
    case INFO_TYPE_NAME:
      return ev3_mode_update_name(info, msg, len);
    case INFO_TYPE_RAW:
      return ev3_mode_update_range(info, RANGE_RAW, msg, len);
    case INFO_TYPE_PCT:
      return ev3_mode_update_range(info, RANGE_PCT, msg, len);
    case INFO_TYPE_SI:
      return ev3_mode_update_range(info, RANGE_SI, msg, len);
    case INFO_TYPE_SYMBOL:
      return ev3_mode_update_symbol(info, msg, len);
    case INFO_TYPE_FORMAT:
      return ev3_mode_update_format(info, msg, len);
    default:
      ev3_warn("received an unknown INFO: header 0x%02hhX, please teach me about it", msg[0]);
      // accept unknown INFO elements: just ignore them
      return EV3_OK;
  }
}

ev3uart_error_code ev3_mode_update_name(ev3_sensor_info *info, const uint8_t *msg, uint32_t len) {
  if (info->extended != NULL) {
    uint32_t mode_num = MODE_NUMBER_OF(msg[0]);

    uint32_t supplied_size = len - EV3_INFO_MSG_FRAME_OVERHEAD;
    // the array is EV3_MAX_SENSOR_NAME_LEN+1, the last char is zero terminator
    uint32_t copy_size = min_u32(supplied_size, EV3_MAX_SENSOR_NAME_LEN);
    memcpy(info->extended->modes[mode_num].name, &msg[2], copy_size);
  }
  return EV3_OK;
}

ev3uart_error_code ev3_mode_update_symbol(ev3_sensor_info *info, const uint8_t *msg, uint32_t len) {
  if (info->extended != NULL) {
    uint32_t mode_num = MODE_NUMBER_OF(msg[0]);

    uint32_t supplied_size = len - EV3_INFO_MSG_FRAME_OVERHEAD;
    // the array is EV3_MAX_SENSOR_SYMBOL_LEN+1, the last char is zero terminator
    uint32_t copy_size = min_u32(supplied_size, EV3_MAX_SENSOR_SYMBOL_LEN);
    memcpy(info->extended->modes[mode_num].unit, &msg[2], copy_size);
  }
  return EV3_OK;
}

ev3uart_error_code
ev3_mode_update_range(ev3_sensor_info *info, ev3_range_type which, const uint8_t *msg, uint32_t len) {
  if (len < sizeof(ev3_info_limits)) {
    ev3_error("received too small INFO_SI/RAW/PCT (got %d B, needed %ld B)", len, sizeof(ev3_info_limits));
    return EV3_ERROR_INVALID_MESSAGE;
  }
  if (info->extended != NULL) {
    ev3_info_limits parsed;
    memcpy(&parsed, msg, sizeof(parsed));
    uint32_t mode_num = MODE_NUMBER_OF(parsed.header);

    switch (which) { // NOLINT(hicpp-multiway-paths-covered)
      case RANGE_SI:
        info->extended->modes[mode_num].si_range.min = parsed.lower;
        info->extended->modes[mode_num].si_range.max = parsed.upper;
        break;
      case RANGE_PCT:
        info->extended->modes[mode_num].pct_range.min = parsed.lower;
        info->extended->modes[mode_num].pct_range.max = parsed.upper;
        break;
      case RANGE_RAW:
        info->extended->modes[mode_num].raw_range.min = parsed.lower;
        info->extended->modes[mode_num].raw_range.max = parsed.upper;
        break;
    }
  }
  return EV3_OK;
}

ev3uart_error_code ev3_mode_update_format(ev3_sensor_info *info, const uint8_t *msg, uint32_t len) {
  if (len < sizeof(ev3_info_format)) {
    ev3_error("received too small INFO_FORMAT (got %d B, needed %ld B)", len, sizeof(ev3_info_limits));
    return EV3_ERROR_INVALID_MESSAGE;
  }

  ev3_info_format parsed;
  memcpy(&parsed, msg, sizeof(parsed));
  uint32_t mode_num = MODE_NUMBER_OF(parsed.header);

  switch (parsed.format) {
    case EV3_FORMAT_INT_8:
    case EV3_FORMAT_INT_16:
    case EV3_FORMAT_INT_32:
    case EV3_FORMAT_FLOAT:
      info->basic.modes[mode_num].data_type = parsed.format;
      break;
    default:
      // we cannot accept unknown values here
      ev3_error("received an unknown value format in INFO_FORMAT: 0x%02hhX", parsed.format);
      return EV3_ERROR_INVALID_MESSAGE;
  }

  info->basic.modes[mode_num].values = parsed.values;
  if (info->extended != NULL) {
    info->extended->modes[mode_num].printf_width = parsed.figures;
    info->extended->modes[mode_num].printf_decimals = parsed.decimals;
  }
  return EV3_OK;
}
