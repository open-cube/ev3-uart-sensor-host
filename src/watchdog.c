/**
 * Helper module for implementing periodic sensor heartbeat.
 *
 * This allows the brick to periodically send NACK messages
 * as sensor watchdog keep-alives. Conversely, if the sensor
 * does not respond to the NACK with new data soon, it is
 * considered to be responding.
 */
#include "ev3/porting/timer.h"
#include "ev3/watchdog.h"
#include "ev3/messages.h"

static bool has_nack_period_elapsed(ev3_watchdog *wd) {
  uint32_t now = ev3_get_millis();
  uint32_t elapsed_ms = (now - wd->msec_of_last_nack);
  return elapsed_ms >= EV3_HEARTBEAT_PERIOD_MS;
}

static void update_miss_counts(ev3_watchdog *wd) {
  if (wd->data_packets_since_nack == 0) {
    wd->nacks_without_data += 1;
  } else {
    wd->nacks_without_data = 0;
  }
  wd->data_packets_since_nack = 0;
}

static bool too_many_missed_nacks(ev3_watchdog *wd) {
  uint32_t threshold;
  if (!wd->ultrasonic_singleshot_workaround) {
    threshold = EV3_MAX_HEARTBEATS_WITHOUT_DATA;
  } else {
    threshold = EV3_MAX_HEARTBEATS_WITHOUT_DATA_LAX;
  }
  return wd->nacks_without_data > threshold;
}

void ev3_watchdog_enter_data(ev3_watchdog *wd) {
  wd->msec_of_last_nack = ev3_get_millis();
  wd->data_packets_since_nack = 0;
  wd->nacks_without_data = 0;
  wd->ultrasonic_singleshot_workaround = false;
}

ev3_watchdog_action ev3_watchdog_update_data(ev3_watchdog *wd) {
  bool send_nack_now = false;
  if (has_nack_period_elapsed(wd)) {
    update_miss_counts(wd);

    wd->msec_of_last_nack = ev3_get_millis();
    send_nack_now = true;
  }

  ev3_watchdog_action ret;
  if (too_many_missed_nacks(wd)) {
    ret = SENSOR_GONE;
  } else if (send_nack_now) {
    ret = SENSOR_OK_SEND_NACK;
  } else {
    ret = SENSOR_OK;
  }
  return ret;
}

void ev3_watchdog_received_data(ev3_watchdog *wd) {
  wd->data_packets_since_nack++;
}

void ev3_watchdog_enter_init(ev3_watchdog *wd) {
  wd->msec_of_cmd_type = ev3_get_millis();
}

ev3_watchdog_action ev3_watchdog_update_init(ev3_watchdog *wd) {
  uint32_t now = ev3_get_millis();
  uint32_t elapsed_ms = (now - wd->msec_of_cmd_type);
  bool timed_out = elapsed_ms >= EV3_HANDSHAKE_TIMEOUT_MS;
  return timed_out ? SENSOR_GONE : SENSOR_OK;
}
