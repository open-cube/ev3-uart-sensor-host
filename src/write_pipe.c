/**
 * Simple queue for sensor write requests in DATA mode
 */
#include <memory.h>
#include <inttypes.h>
#include "ev3/write_pipe.h"
#include "ev3/porting/debug.h"

static bool queue_contains_data(ev3_write_pipe *wq);
static void try_refill_with_requests(ev3_write_pipe *wq);

void write_pipe_reset(ev3_write_pipe *wq) {
  wq->tx_done = 0;
  wq->tx_needed = 0;
  wq->mode_requested = EV3_WQ_MODE_INVALID;
  wq->nack_requested = false;
  wq->ack_requested = false;
}

bool write_pipe_is_empty(ev3_write_pipe *wq) {
  return !queue_contains_data(wq) &&
         !wq->nack_requested &&
         !wq->ack_requested &&
         wq->mode_requested == EV3_WQ_MODE_INVALID;
}

bool queue_contains_data(ev3_write_pipe *wq) {
  return wq->tx_done != wq->tx_needed;
}

void write_pipe_push_nack(ev3_write_pipe *wq) {
  wq->nack_requested = true;
}

void write_pipe_push_ack(ev3_write_pipe *wq) {
  wq->ack_requested = true;
}

void write_pipe_push_mode_switch(ev3_write_pipe *wq, uint8_t mode) {
  wq->mode_requested = mode;
}

ev3uart_error_code write_pipe_push_buffer(ev3_write_pipe *wq, const uint8_t *data, uint32_t len) {
  if (len > EV3_MSG_BUFFER_SIZE) {
    // fail big writes
    ev3_error("attempted to send an explicit message of size %" PRIu32 " larger than max %u", len, EV3_MSG_BUFFER_SIZE);
    return EV3_ERROR_INVALID_MESSAGE;
  }

  if (queue_contains_data(wq)) {
    // tx buffer is not free, try again later
    ev3_debug("attempted to send an explicit message while the TX buffer was occupied");
    return EV3_ERROR_BUSY;
  }

  // tx buffer is free, put the data directly in there
  memcpy(wq->tx_buffer, data, len);
  wq->tx_needed = len;
  wq->tx_done = 0;
  return EV3_OK;
}

ev3uart_error_code write_pipe_poll(ev3_write_pipe *wq, ev3_uart_handle uart) {
  int32_t sent = 1;
  while (sent > 0) {
    if (!queue_contains_data(wq)) {
      try_refill_with_requests(wq);
    }
    if (queue_contains_data(wq)) {
      sent = ev3_uart_write_block(uart, wq->tx_buffer, wq->tx_needed, &wq->tx_done);
    } else {
      sent = 0;
    }
  }

  return sent;
}


void try_refill_with_requests(ev3_write_pipe *wq) {
  if (wq->ack_requested) {
    wq->tx_buffer[0] = MSG_ACK;
    wq->tx_needed = 1;
    wq->tx_done = 0;
    wq->ack_requested = false;

  } else if (wq->nack_requested) {
    wq->tx_buffer[0] = MSG_NACK;
    wq->tx_needed = 1;
    wq->tx_done = 0;
    wq->nack_requested = false;

  } else if (wq->mode_requested != EV3_WQ_MODE_INVALID) {
    wq->tx_buffer[0] = MSG_TYPE_CMD_SELECT | MSG_LEN_1;
    wq->tx_buffer[1] = wq->mode_requested;
    wq->tx_buffer[2] = wq->tx_buffer[0] ^ wq->tx_buffer[1] ^ 0xFF;
    wq->tx_needed = 3;
    wq->tx_done = 0;
    wq->mode_requested = EV3_WQ_MODE_INVALID;
  }
}
